Executable=launch_ARRANGE.sh
arguments="$Fnx(FILES)"
output=./joblogs/myjob.o$(Cluster)-$(Process)
error=./joblogs/myjob.e$(Cluster)-$(Process)
log=./joblogs/myjob.l$(Cluster)-$(Process)
request_memory=1024MB
notify_user=harry.moss@sheffield.ac.uk
notification=always
#requirements=Memory>2000
#rank=Memory
maxjobretirementtime=24*(3600)
queue FILES matching files ./inputs/*.in
