#!/usr/bin/python
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from tqdm import tqdm

import random # needed for (pseudo)random number generation in python

verbose=False
# Some housekeeping for testing on the cluster - delete when done with

if(verbose):
	print("Arguments provided \n [1]: {} \t [2]: {}".format(str(sys.argv[1]),str(sys.argv[2])))

###################
# _____            _   _ _____   ____  __  __ _____  _____ ______ _____  
#|  __ \     /\   | \ | |  __ \ / __ \|  \/  |_   _|/ ____|  ____|  __ \ 
#| |__) |   /  \  |  \| | |  | | |  | | \  / | | | | (___ | |__  | |__) |
#|  _  /   / /\ \ | . ` | |  | | |  | | |\/| | | |  \___ \|  __| |  _  / 
#| | \ \  / ____ \| |\  | |__| | |__| | |  | |_| |_ ____) | |____| | \ \ 
#|_|  \_\/_/    \_\_| \_|_____/ \____/|_|  |_|_____|_____/|______|_|  \_\
##################

########################################
# Load the two input files. The first one is the collection of secondary muons (the output from corsika) with the seven column format (px, py, px, p, x, y, z). The second file corresponds to the data of the distances traveled in rock for a given observation point and for the zenith and azimuthal angles $\theta$ and $\phi$.
########################################
#input paths:
########################################x
### 84 days CORISKA split into several inputs - use these
infileName=str(sys.argv[1])
if(verbose):
    print("Reading input file: {}".format(infileName))
muons=pd.read_csv(infileName,delimiter=' ',names=["px","py","pz","p","x","y","z"],skiprows=0) 
if(verbose):
    print("input DF length: {}".format(len(muons.index)))
depth=pd.read_csv('distances_Dec2018.txt',delimiter='\s+',names=["theta","phi","d","x0","y0","z0"],skiprows=0)
if(verbose):
    print("distances file length: {}".format(len(depth.index)))
if(verbose):
    print("Loaded files")

########################################
# The energy of the muon is calculated using the momenta information as $E=\sqrt{p^2+m_{mu}^2}$ where $p$ is the momentum and $m_{mu}$ is the muon rest mass. We also calculate the directions for each muon such that its zenith angle is $\theta=\arccos(p_z/p)$ and its azimuthal angle is $\varphi=\arctan(p_y/p_z)$.
########################################


m_mu=0.1057
E=np.sqrt(muons.p**2+m_mu**2)
theta=np.arccos(muons.pz/muons.p)
phi=np.arctan2(muons.py,muons.px)

def round_of_rating(number):
    return np.round(number*2.)/2.

phi=np.where(phi<0 , 2*np.pi+phi, phi) # changes phi list to array here I think...
theta_d=round_of_rating(np.degrees(theta))
phi_d=round_of_rating(np.degrees(phi))




########################################
# Both input files are combined, augmenting the CORSIKA muon data to additionally include the corresponding distance to each pair of zenith-azimuth angles. For each line of both files where $\theta$ and $\varphi$ coincide then the distance to this corresponding tuple and muon energy will be added.
########################################

data=np.c_[theta_d,phi_d,E]#,muons.x,muons.y,muons.z]
#index = ['Row'+str(i) for i in range(1, len(data)+1)]
data=pd.DataFrame(data,columns=["theta","phi","Ener"]) # no need to merge, you match coords later

result=data.sort_values('theta')
if(verbose):
    print("length of trimmed CORSIKA DF: {}".format(len(result.index)))


result_new=result[result.theta>=66] # YOU DO NEED THIS LINE! It ensures that result_new (the augmented file) will have the same length as the corsika input file when the angular restrains above (117<= phi <= 147 and 66<= theta <= 84) are imposed! 

coordRanges=pd.read_csv('AngularCoordDistances_AllAngles.txt',delimiter='\t',names=["theta","phi","X","Y","Z","S","D"],skiprows=1) # need to skip the first 2 rows because they're commented

#result_minDist=result_new[result_new.d>=10.] # have some sort of filter so that >10m of rock has to be traversed
#result_d=np.c_[result_minDist.theta,result_minDist.phi,result_minDist.x0,result_minDist.y0,result_minDist.z0,result_minDist.Ener,result_minDist.d] # copy of result_m but for degs
#result_d=pd.DataFrame(result_d,columns=["theta","phi","x0","y0","z0","Ener","d"]
#result_XYZranges=result_d.copy()
# the above is incorrect logic

result_XYZranges=result_new.copy()
if(verbose):
    result_XYZranges.to_csv("testingDF_in.txt",sep='\t')
thetaRange=np.arange(result_XYZranges['theta'].min(),result_XYZranges['theta'].max()+0.5,0.5) # same angular range as in coordRanges...

modifiedCoordMuons=pd.DataFrame(columns=["theta","phi","x0","y0","z0","Ener","d","norm"])
randomEntriesDF=pd.DataFrame(columns=["theta","phi","x0","y0","z0","Ener","d","norm"])


if(verbose):
    print("Length of result_XYZranges: {}".format(len(result_XYZranges.index)))
GoodRandoms=0

for angle in tqdm(thetaRange):
    allowedVals=coordRanges.loc[coordRanges['theta']==angle] # this finds the 'theta' in the coordRanges file
    phi_min=allowedVals['phi'].min()
    phi_max=allowedVals['phi'].max()
    chosenPhi=-999
    for i, row in result_XYZranges.iterrows(): # so now loops over the corsika muons...
        if result_XYZranges.at[i,'theta']==angle:
            maxRandomisations=100
            successes=0
            counter=1
            for j in range(counter,maxRandomisations+1):
                randomPhi=round_of_rating(random.uniform(0,361)) # finally find a random value of phi, rounds it to nearest half degre
                if(phi_min<=randomPhi and phi_max>=randomPhi):
                    chosenPhi=randomPhi
                    filterFrame = allowedVals[(allowedVals['theta'] == angle) & (allowedVals['phi'] == chosenPhi)] # makes a dataframe with only the allowed rows in...
                    if not filterFrame.empty:
                        rows=np.random.choice(filterFrame.index.values,1) # randomly select a row from filterFrame and assign 'rows' the value of the index            
                        sampledFrame=filterFrame.loc[rows] # find the values of all columns at this index
                        rPhi=sampledFrame.iloc[0]['phi'] # alternatively use chosenPhi here...
                        rx0=sampledFrame.iloc[0]['X']
                        ry0=sampledFrame.iloc[0]['Y']
                        rz0=sampledFrame.iloc[0]['Z']
                        rD=sampledFrame.iloc[0]['D']
                        rEner=result_XYZranges.at[i,'Ener']
                        randomRow=pd.DataFrame([[angle,rPhi,rx0,ry0,rz0,rEner,rD,maxRandomisations]],columns=["theta","phi","x0","y0","z0","Ener","d","norm"])
                        randomEntriesDF=randomEntriesDF.append(randomRow,ignore_index=True)
                        successes=successes+1
                counter=counter+1
                if(counter==maxRandomisations+1 and successes>0):
                    GoodRandoms=GoodRandoms+successes
		    if(verbose):
			    print("End of randomisations for theta={} \t Found {} successful random angular combinations in {} trials".format(angle,successes,maxRandomisations))
                elif(verbose and counter==maxRandomisations+1 and successes==0):
                    print("Found ZERO successful angular combinations in {} trials for angle {}".format(maxRandomisations,angle))
            

print("Found {} successful angular combinations from randomisation".format(GoodRandoms))
modifiedCoordMuons=randomEntriesDF
if(verbose):
    modifiedCoordMuons.to_csv("testingDF_out.txt",sep='\t')
########################################
# For MUSIC the input expects the angles in radians, the energies in GeV and the distances in centimeters, then we will prepare the input file for MUSIC finally.
########################################


print("Doing post-processing")

phi_m=modifiedCoordMuons.phi*np.pi/180. # Angle in radians
theta_m=modifiedCoordMuons.theta*np.pi/180. # Angle in radians
x_m,y_m,z_m,d_m=modifiedCoordMuons.x0*100,modifiedCoordMuons.y0*100,modifiedCoordMuons.z0*100,modifiedCoordMuons.d*100 # Distances in cm

result_m=np.c_[theta_m,phi_m,x_m,y_m,z_m,modifiedCoordMuons.Ener,d_m] # Concatenated data for the augmented MUSIC input
result_m=pd.DataFrame(result_m,columns=["theta","phi","x0","y0","z0","Ener0","d"]) # convert to pandas dataframe

OriginalOutfileName=str(sys.argv[2])
baseOutfileName=OriginalOutfileName.split('.',1)[0]
outfileName=baseOutfileName+".dat"
print("Eventual outfileName: {}".format(outfileName))
result_m.to_csv(outfileName,sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d


