#!/usr/bin/python

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from tqdm import tqdm

import random # needed for (pseudo)random number generation in python
########################################
#input paths:
########################################

# Attempt one (incorrect but left for legacy code)
#modifiedCoordMuons=pd.read_csv('result_XYZranges.df',delimiter=' ',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1) # need to skip the first row (header)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_correctPhi_21Day.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d","PhiNormalisation"],skiprows=1) # need to skip the first row (header)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_correctPhi_Day_36_to_49.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d","PhiNormalisation"],skiprows=1) # need to skip the first row (header)



#modifiedCoordMuons=pd.read_csv('result_XYZranges_FixedPhi_Day_1_to_14.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_FixedPhi_Day_15_to_35.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_FixedPhi_Day_36_to_49.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_FixedPhi_Day_50_to_84.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
 

modifiedCoordMuons=pd.read_csv('result_XYZranges_RandomisedPhi_Day_1_to_14.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_RandomisedPhi_Day_15_to_35.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_RandomisedPhi_Day_36_to_49.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)
#modifiedCoordMuons=pd.read_csv('result_XYZranges_RandomisedPhi_Day_50_to_84.df',delimiter='\t',names=["theta","phi","x0","y0","z0","Ener","d"],skiprows=1)

########################################
# For MUSIC the input expects the angles in radians, the energies in GeV and the distances in centimeters, then we will prepare the input file for MUSIC finally.
########################################

phi_m=modifiedCoordMuons.phi*np.pi/180. # Angle in radians
theta_m=modifiedCoordMuons.theta*np.pi/180. # Angle in radians
x_m,y_m,z_m,d_m=modifiedCoordMuons.x0*100,modifiedCoordMuons.y0*100,modifiedCoordMuons.z0*100,modifiedCoordMuons.d*100 # Distances in cm

result_m=np.c_[theta_m,phi_m,x_m,y_m,z_m,modifiedCoordMuons.Ener,d_m] # Concatenated data for the augmented MUSIC input
result_m=pd.DataFrame(result_m,columns=["theta","phi","x0","y0","z0","Ener0","d"]) # convert to pandas dataframe

################################# 
#Finally save the data on the disk that will be used as the input of MUSIC. The file has no header, therefore remember that the format of the data is $[\theta_0,\varphi_0,x,y,z,E_0,d]$.
#################################

# Use these if you've selected the fixedphi setup in ARRANGE_MUSIC_PHASEONE.py...
#result_m.to_csv('Input_MusicMuons_Jan2018_FixedPhi_Day_1_to_14.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d
#result_m.to_csv('Input_MusicMuons_Jan2018_FixedPhi_Day_15_to_35.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d
#result_m.to_csv('Input_MusicMuons_Jan2018_FixedPhi_Day_36_to_49.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d
#result_m.to_csv('Input_MusicMuons_Jan2018_FixedPhi_Day_50_to_84.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d


# Use these if you've selected the randomised phi setup in ARRANGE_MUSIC_PHASEONE.py...
result_m.to_csv('Input_MusicMuons_Jan2018_RandomPhi_Day_1_to_14.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d
#result_m.to_csv('Input_MusicMuons_Jan2018_RandomPhi_Day_15_to_35.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d
#result_m.to_csv('Input_MusicMuons_Jan2018_RandomPhi_Day_36_to_49.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d
#result_m.to_csv('Input_MusicMuons_Jan2018_RandomPhi_Day_50_to_84.dat',sep=' ',index=False,mode='w',header=False) # write to file! format is theta,phi,x0,y0,z0,energy0,d


