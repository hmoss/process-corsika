#!/bin/bash
#================================================================================
# Program for split MUSIC input into several input with N lines for launch MUSIC
# useage for example
# ./split_file.sh input.file
#================================================================================
N=880000 # gets you about 700 jobs for 14 day
#N=10
#input=$1
split -l $N --numeric-suffixes -a 4 $1 $2

# use of $2 adds optional outname

for file in $2*
do
    mv "$file" "$file.in"
done