#!/bin/sh                                                                                                                                                                                                                                  
hostname
source /opt/rh/python27/enable
source /home/hmoss/MuonTomography/pythonenv/bin/activate

ARRANGEInput=${1}
ARRANGEInput=${ARRANGEInput##*inputs/}

#ARRANGEInputNumbers=${ARRANGEInput##*_}
#ARRANGEOutputNumbers=${ARRANGEInputNumbers%.in}

ARRANGEOutputNumbers=${ARRANGEInput##_}

ARRANGEOutputDir=${ARRANGEOutputNumbers%.in}

#ARRANGEOutput=${ARRANGEOutputNumbers}.out
ARRANGEOutput=${ARRANGEOutputDir}.out

echo ${ARRANGEOutput}

relativeInputPath="inputs"
runDir=${PWD}

#ARRANGEOutputPath="/data/hmoss/MuonTomography/grid/November2018/CORSIKAMuonsOut/RandomisedPhi/One/Cluster/outputs"
ARRANGEOutputPath="./outputs"

#cd ${runDir}
mkdir -p -v ${ARRANGEOutputPath}

scratchDir="NULLDIRECTORY_NOTFOUND_HMOSS"

# If scratch available update outputpath to save files there, otherwise kill job.                                                                                                                                                                                            
if [ -d "/scratch/hmoss/" ]
then
mkdir -p /scratch/hmoss/ARRANGERUN_${ARRANGEOutputDir}
scratchDir=/scratch/hmoss/ARRANGERUN_${ARRANGEOutputDir}
else
echo "broken?"
exit
fi

# Run the job
echo "I am in $PWD"
echo "Running JOB in $runDir"
echo "Running: python ARRANGE_MUSIC_CLUSTER.py $relativeInputPath/$ARRANGEInput $scratchDir/$ARRANGEOutput"
python ARRANGE_MUSIC_CLUSTER.py ${relativeInputPath}/${ARRANGEInput} ${scratchDir}/${ARRANGEOutput}

#  so runs jobs on /home/ and shunts output files to /scratch/
# Now copy the outputs from scratch back to the inputpath
echo "Moving files from $scratchDir to $ARRANGEOutputPath..."
mv -v ${scratchDir}/*.dat ${ARRANGEOutputPath}
# Remove the temp scratch folder we made
if [ -d "$scratchDir" ]
then
    rm -rv ${scratchDir}
fi

cd ${runDir} # return nicely back to the run directory